import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  @Input() users: string[] = [];
  @Output() buttonClicked = new EventEmitter<string>();

  @ViewChild('input') inputElement: ElementRef;

  constructor() { }

  ngOnInit(): void {
  }

  public onButtonClicked(e: MouseEvent) {
    this.buttonClicked.emit(e.target['text']);
  }

  // public textEntered(i: HTMLInputElement) {
  //   console.log(i.value);
  // }

  public textEntered() {
    console.log(this.inputElement.nativeElement.value);
  }

}
