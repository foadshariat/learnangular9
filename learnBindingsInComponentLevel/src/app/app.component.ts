import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'learnBindingsInComponentLevel';
  usersFromParent: string[] = ['Foad', 'Rezvan', 'Milad'];

  public onClick() {
    
  }

  public verified(e: string) {
    console.log('From Parent: ' + e);
  }
}
