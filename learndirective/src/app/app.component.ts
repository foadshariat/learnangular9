import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  hasVisitor:boolean = true;
  users: string[] = ['Foad', 'Rezvan'];

  public onClick() {
    this.users.push('user');
  }
}
