import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  userStatus: string = '';
  constructor() { }

  ngOnInit() {
    this.userStatus = Math.random() > 0.5 ? 'Active' : 'Deactive';
  }

  public getColor() {
    return this.userStatus === 'Active' ? 'green' : 'red';
  }

}
