import {
  Component,
  OnInit,
  Input,
  OnChanges,
  DoCheck,
  AfterContentInit,
  AfterContentChecked,
  AfterViewInit,
  AfterViewChecked,
  OnDestroy
} from '@angular/core';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent
  implements
    OnInit,
    OnChanges,
    DoCheck,
    AfterContentInit,
    AfterContentChecked,
    AfterViewInit,
    AfterViewChecked,
    OnDestroy {
  @Input() companies: string[] = [];

  constructor() {
    console.log('From Constructor');
  }

  public onClick() {
    console.log('Button Clicked');
  }

  ngOnChanges() {
    console.log('From ngOnChanges');
  }

  ngOnInit(): void {
    console.log('From ngOnInit');
  }

  ngDoCheck() {
    console.log('From ngDoCheck');
  }

  ngAfterViewChecked(): void {
    console.log('From ngAfterViewChecked');
  }

  ngAfterViewInit(): void {
    console.log('From ngAfterViewInit');
  }

  ngAfterContentChecked(): void {
    console.log('From ngAfterContentChecked');
  }

  ngAfterContentInit(): void {
    console.log('From ngAfterContentInit');
  }

  ngOnDestroy(): void {
    console.log('From ngOnDestroy');
  }
}
