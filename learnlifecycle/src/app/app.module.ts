import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CompanyComponent } from './company/company.component';
import { HomeComponent } from './home/home.component';

import { Routes, RouterModule } from '@angular/router';
import { WorkComponent } from './work/work.component';

const appRoutes: Routes = [
  { path: '',  component: HomeComponent},
  { path: 'work', component: WorkComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    CompanyComponent,
    HomeComponent,
    WorkComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
